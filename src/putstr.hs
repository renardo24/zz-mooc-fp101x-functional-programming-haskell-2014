putStr1 [] = return ""
putStr1 (x : xs) = putChar x >> putStr1 xs

putStr2 [] = return ()
putStr2 (x : xs) = putChar x >> putStr2 xs

--putStr3 [] = return ()
--putStr3 (x : xs) = putChar x >>= putStr3 xs

--putStr4 [] = return ()
--putStr4 (x : xs) = putStr4 xs >>= putChar x
