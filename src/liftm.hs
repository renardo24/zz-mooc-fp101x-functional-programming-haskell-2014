liftM1 f m
  = do x <- m
       return (f x)

liftM2 f m = m >>= \ a -> f a

liftM3 f m = m >>= \ a -> return (f a)

liftM4 f m = return (f m)

liftM5 f m = m >>= \ a -> m >>= \ b -> return (f a)

liftM6 f m = m >>= \ a -> m >>= \ b -> return (f b)

liftM7 f m = mapM f [m]

liftM8 f m = m >> \ a -> return (f a)
