-- can be invoked as
-- *Main> :t Node5 (Leaf5 1) (Leaf5 2)
-- Node5 (Leaf5 1) (Leaf5 2) :: Tree5

-- The expression Node (Leaf 1) (Leaf 2) is a value of the datatype:
data Tree1 = Node1 | Leaf1 | Int
data Tree2 = Leaf2 Int | Node2 Int Int
data Tree3 = Leaf3 Tree3 | Node3 Int Int
data Tree4 = Leaf4 Tree4 | Node4 Tree4 Tree4
data Tree5 = Leaf5 Int | Node5 Tree5 Tree5
