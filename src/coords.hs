-- Using a list comprehension, define a function coords :: Integer -> Integer -> [(Integer, Integer)]
-- that returns a list of all coordinate pairs on an [0..m] × [0..n] rectangular grid, where m and n are non-bottom Integers >= 0.
-- Example:
-- coords 1 1 = [(0,0), (0,1), (1,0), (1,1)]
-- coords 1 2 = [(0,0), (0,1), (0,2), (1,0), (1, 1), (1, 2)]
coords m n
    | m < 0 = error "m cannot be less than 0"
    | n < 0 = error "n cannot be less than 0"
    | m == 0 && n > 0 = coords 0 (n-1) ++ [(m, n)]
    | n == 0 && m > 0 = coords (m-1) 0 ++ [(m, n)]
    | m == 0 && n == 0 = [(0, 0)]
    | otherwise = coords (m-1) (n-1) ++ [(m, n)]


coords' m n = [(a, b) | a <- [0..m], b <- [0..n]]
