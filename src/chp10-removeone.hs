-- removeone :: Eq a +. a -> [a] -> [a]
-- usage
-- *Main> removeone4 2 [1,2,3,2]
-- [1,3,2]

removeone1 x [] = [x]
--removeone1 x ys
--  | x == head ys = ys
--  | otherwise = y : removeone1 x ys

removeone2 x [] = []
removeone2 x (y : ys)
  | x == y = ys
  | otherwise = x : removeone2 x ys

removeone3 x [] = []
removeone3 x ys
  | x == head ys = ys
  | otherwise = removeone3 x ys

removeone4 x [] = []
removeone4 x (y : ys)
  | x == y = ys
  | otherwise = y : removeone4 x ys
