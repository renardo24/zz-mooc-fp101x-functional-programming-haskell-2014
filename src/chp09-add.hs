-- can be invoked as
-- *Main> :t Add3 (Val3 1) (Val3 2)
-- Add3 (Val3 1) (Val3 2) :: Expr3

-- The expression Add (Val 1) (Val 2) is a value of the datatype:
data Expr1 = Add1 | Val1 | Int
data Expr2 = Add2 Int Int | Val2 Int
data Expr3 = Add3 Expr3 Expr3 | Val3 Int
data Expr4 = Add4 Int Int | Val4 Expr4
data Expr5 = Add5 Expr5 Expr5 | Val5 Expr5
