triangle :: Integer -> Integer
triangle 0 = 0
triangle n = n + triangle (n - 1)
