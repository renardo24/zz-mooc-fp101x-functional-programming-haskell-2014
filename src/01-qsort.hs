--qsort [] = []
--qsort (x : xs) = qsort larger ++ [x] ++ smaller
--  where smaller = [a | a <- xs, a <= x]
--        larger = [b | b <- xs, b > x]

--qsort [] = []
--qsort (x : xs) = reverse (qsort smaller ++ [x] ++ larger)
--  where smaller = [a | a <- xs, a <= x]
--        larger = [b | b <- xs, b > x]

--qsort [] = []
--qsort xs = qsort larger ++ qsort smaller ++ [x]
--  where x = minimum xs
--        smaller = [a | a <- xs, a <= x]
--        larger = [b | b <- xs, b > x]

--qsort [] = []
--qsort (x : xs) 
--  = reverse (qsort smaller) ++ [x] ++ reverse (qsort larger)
--  where smaller = [a | a <- xs, a <= x]
--        larger = [b | b <- xs, b > x]

--qsort [] = []
--qsort (x : xs) = qsort larger ++ [x] ++ smaller
--  where larger = [a | a <- xs, a > x || a == x]
--        smaller = [b | b <- xs, b < x]

--qsort [] = []
--qsort (x : xs) = qsort larger ++ [x] ++ smaller
--  where smaller = [a | a <- xs, a < x]
--        larger = [b | b <- xs, b > x]

qsort [] = []
qsort (x : xs) 
  = reverse
      (reverse (qsort smaller) ++ [x] ++ reverse (qsort larger))
  where smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

--qsort [] = []
--qsort xs = x : qsort larger ++ smaller
--  where x = maximum xs
--        smaller = [a | a <- xs, a < x]
--        larger = [b | b <- xs, b >= x]

