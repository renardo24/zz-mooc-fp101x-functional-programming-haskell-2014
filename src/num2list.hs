f x
  | x < 10 = [x]
  | x == 0 = [] 
  | otherwise  = f (x `div` 10) ++ [x `mod` 10]
