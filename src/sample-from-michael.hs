doubleEveryOther n = [((y-1) `mod` 2 + 1 ) * x|(x,y)<-zip n [1 .. length n]]

doubleEveryOther2 n = [((z - 1) `mod` 2 + 1 ) * (n !! (z - 1))|z <- [1 .. length n]]

doubleSecond2 []     = []   
doubleSecond2 [n]    = [n]  
doubleSecond2 (n:ns) = [n,  head ns * 2 ] ++ doubleSecond2  (tail ns)


doubleSecond3 []     = []   
doubleSecond3 (n:[])    = [n]  
doubleSecond3 (n:m:ms) = [n,  m * 2 ] ++  doubleSecond3  ms

doubleSecond4 []     = []   
doubleSecond4 (n:[])    = [n]  
doubleSecond4 (n:m:ms) = zipWith  (*) [n,  m ] [1,2] ++  doubleSecond4  ms

doubleSecond5 []     = []   
doubleSecond5 (n:[])    = [n]  
doubleSecond5 (n:m:ms) = zipWith  (+) [n,  m ] [0,m] ++  doubleSecond5  ms

doublePairs6 n
             | length n == 0 = []
             | (length n) `mod` 2 == 0 =  2 * (head n) : doublePairs6 (tail n) 
             | otherwise = head n : doublePairs6 (tail n)

-- This one works straight from the number
doublePairs :: (Integral a) => a -> [a]
doublePairs n
              | n < 10 = [n]
              | n == 0 = [] 
              | n > 9 = ( (n `mod` 100 ) `mod` 10 : [((n `mod` 100 ) `div` 10 ) * 2] ) ++ doublePairs (n `div` 100) 
              | otherwise = [n]