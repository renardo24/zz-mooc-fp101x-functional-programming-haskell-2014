-- to use this, you can define this function
-- let f s = "Hello " ++ s
-- and it xan be called like this
-- f "Olivier"
-- output: "Hello Olivier"
interact1 :: (String -> String) -> IO()
interact1 f
  = do input <- getLine
       putStrLn (f input)

interact2 :: (String -> String) -> IO()
interact2 f
  = do input <- getLine
       putStrLn input

--interact3 :: (String -> String) -> IO()
interact3 f
  = do input <- getChar
       putStrLn (f input)

interact4 :: (String -> String) -> IO()
interact4 f
  = do input <- getLine
       putStr (f input)
