squares n
    | n < 0 = error "Must be equal to or greater than 0"
    | n == 0 = []
    | otherwise = squares (n - 1) ++ [(n * n)]

sumSquares n = sum (squares n)
