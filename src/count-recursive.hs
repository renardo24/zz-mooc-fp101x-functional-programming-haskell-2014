count :: Eq a => a -> [a] -> Integer
count _ [] = 0
count a (x:xs) = if a == x then (1 + count a xs) else count a xs
