-- [Int] -> (Int -> [Int]) -> [Int]
-- *Main> digitiseAll [111, 123, 4, 5]
-- [1,1,1,1,2,3,4,5]

digitise :: Int -> [Int]
digitise x
    | x == 0 = []
    | x > 0 = digitise (x `div` 10) ++ [x `mod` 10]
 
digitiseAll :: [Int] -> [Int]
digitiseAll [] = []
digitiseAll (x:xs) = digitise (x) ++ digitiseAll (xs)
