digitise :: Int -> [Int]
digitise x 
    | x == 0 = []
    | x > 0 = digitise (x `div` 10) ++ [x `mod` 10]

