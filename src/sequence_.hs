sequence_1 [] = return []
--sequence_1 (m : ms) = m >> \ _ -> sequence_1 ms

sequence_2 [] = return ()
sequence_2 (m : ms) = (foldl (>>) m ms) >> return ()

sequence_3 ms = foldl (>>) (return ()) ms

sequence_4 [] = return ()
sequence_4 (m : ms) = m >> sequence_4 ms

sequence_5 [] = return ()
sequence_5 (m : ms) = m >>= \ _ -> sequence_5 ms

--sequence_6 ms = foldr (>>=) (return ()) ms

sequence_7 ms = foldr (>>) (return ()) ms

sequence_8 ms = foldr (>>) (return []) ms

