squares' m n
    | m < 0 = error "m cannot be less than 0"
    | n < 0 = error "n cannot be less than 0"
    | m == 0 = []
    | otherwise = [(n+1) * (n+1)] ++ squares' (m-1) (n+1)
