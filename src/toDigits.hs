digits 0 = []
--digits x = digits (x `div` 10) ++ [x `mod` 10]
digits x = [x `mod` 10] ++ digits (x `div` 10)
--digits x = [ x!!n * 2 | n <- [1..length(x)], n `mod` 2 == 0]
double xs = [f x | x <- digits xs | f <- cycle [id, (*2)]]
